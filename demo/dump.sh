#!/bin/bash
#
# Standard data dump
#
cd "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/..

mkdir -p ./demo/fixtures/

./demoenv/bin/python demo/manage.py dumpdata --indent=2 -e admin.logentry -e auth.permission -e contenttypes -e sessions -e menus > ./demo/fixtures/demodata.json

